﻿Imports cwbx
Imports System.Xml

Public Class Search
    'ByRef as400 As cwbx.AS400System
    Public Function SetupPolicyConnection(ByVal sType As String) As cwbx.Program
        'File.WriteAllText("c:\XMLs\" + "LogFile.txt", "Top of Function SetupPolicyConnection " + sType)
        Dim program As cwbx.Program = New cwbx.Program()
        Dim sErr As String = ""
        Dim sLoginMode As String = My.Settings.LogInMode
        Dim as400system As New cwbx.SystemNames
        Dim AS400Server As String = "" 'AS400 server name
        Dim AS400User As String = "" ' User name to sign on to AS/400
        Dim AS400Pgm As String = "" 'Name of program you wish to call on the AS/400
        Dim AS400Lib As String = "" ' Name of library where the program is located
        Dim AS400IP As String = "" 'TEST-172.23.2.171" PROD-172.23.2.201  DR-172.28.2.206
        Dim cmd As cwbx.Command = New cwbx.Command()
        Dim sConnectInfo As String

        Try
            Dim AS400Password As String = "" ' Password used to sign on to the AS/400 lju8okjg claimsdev1

            ' File.WriteAllText("c:\XMLs\" + "LogFile.txt", "SetupPolicyConnection " + sType)
            If sLoginMode = "PRODUCTION" Then
                AS400Server = "S10A3739" '"TEST400" S10A3739
                AS400User = "ClaimsID" ' User name to sign on to AS/400
                AS400Password = "lju8okjg" ' Password used to sign on to the AS/400 lju8okjg claimsdev1
                AS400IP = "172.23.2.201" 'TEST-172.23.2.171" PROD-172.23.2.201  DR-172.28.2.206
                AS400Lib = "PGCPGMS"
            ElseIf sLoginMode = "UAT" Then
                AS400Server = "TEST400"
                AS400User = "ClaimsID"
                AS400Password = "claimsdev1"
                AS400IP = "172.23.2.171"
                AS400Lib = "TESTGW"
            ElseIf sLoginMode = "DR" Then
                AS400Server = "S10A3739"
                AS400User = "ClaimsID"
                AS400Password = "lju8okjg"
                AS400IP = "172.28.2.206"
                AS400Lib = "PGCPGMS" 'todo figure out what is down there
            ElseIf sLoginMode = "TEST" Then
                AS400Server = "TEST400"
                AS400User = "ClaimsID"
                AS400Password = "claimsdev1"
                AS400IP = "172.23.2.171"
                AS400Lib = "TESTMS" 'todo figure out new system John was referring to
            ElseIf sLoginMode = "DEV" Then  ''' THIS IS JUST FOR RENEE'S TESTING ONLY
                AS400Server = "TEST400"
                AS400User = "ClaimsID"
                AS400Password = "claimsdev1"
                AS400IP = "172.23.2.171"
                AS400Lib = "TESTBCY" 'todo figure out new system John was referring to
            End If

            If sType = "Search" Then
                AS400Pgm = "GWCFNDPOL"
            ElseIf sType = "Retrieve" Then
                AS400Pgm = "GWCRTVPOL"
            End If
            sErr = sLoginMode & " " & AS400Server & " " & AS400User & " " & AS400Pgm & " " & AS400IP & " " & AS400Lib

            Dim as400 As cwbx.AS400System

            as400 = New AS400System 'Class()

            as400.Define(AS400Server)
            as400.UserID = AS400User
            as400.Password = AS400Password
            as400.IPAddress = AS400IP

            'File.WriteAllText("c:\XMLs\" + "LogFile.txt", "Before Signon  " & as400.UserID & " " & AS400Pgm & " " & AS400Lib)
            as400.Signon()
            as400.Connect(cwbcoServiceEnum.cwbcoServiceRemoteCmd)
            ' PRODUCTION works with block commented out... but does not work with it uncommented.
            'TEST has to have the TESTGW and TESTGWTRN to find policies, without it it pulls nothing
            If sLoginMode = "TEST" Then
                With cmd
                    .system = as400
                    .Run("ADDLIBLE TESTMS")
                    .Run("ADDLIBLE TESTMSTRN")
                    .Run("ADDLIBLE TESTGW")
                End With
            ElseIf sLoginMode = "UAT" Then
                With cmd
                    .system = as400
                    .Run("ADDLIBLE TESTGW")
                    .Run("ADDLIBLE TESTGWTRN")
                    '.Run("ADDLIBLE PGCPGMS")
                End With
            ElseIf sLoginMode = "DEV" Then
                With cmd
                    .system = as400
                    .Run("ADDLIBLE TESTBCY")  ' THIS IS DEV TEST POLICY DATA
                    .Run("ADDLIBLE TESTMS") ' THIS IS WHERE THE PROGRAMS ARE AT                     
                End With
            End If

            If as400.IsConnected(cwbcoServiceEnum.cwbcoServiceRemoteCmd) = 1 Then  ' did we make a good connection to the 400
                'File.WriteAllText("c:\XMLs\" + "LogFile.txt", "IsConnected")
                program.system = as400
                program.LibraryName = AS400Lib
                program.ProgramName = AS400Pgm
                sConnectInfo = Convert.ToString(cwbcoServiceEnum.cwbcoServiceRemoteCmd) & " " & Convert.ToString(program.system) & " " & Convert.ToString(program.LibraryName) & " " & Convert.ToString(program.ProgramName)
                LogToDB(ClassName + "." + MethodName, "iSeries Connecting ", "Function call", "", CType(LogLevel.Informational, String), "Connection " & sConnectInfo, CType(Form1.sParam, String) + "/" + Environment.UserName)
            End If

        Catch ex As Exception
            LogToDB(ClassName + "." + MethodName, "iSeries connection ", "Function call", "", CType(LogLevel.Error, String), "Exception thrown " & "SetupPolicyConnection" & Convert.ToString(cwbcoServiceEnum.cwbcoServiceRemoteCmd) & ex.Message, Form1.sParam + "/" + Environment.UserName)
            Throw New ArgumentException(String.Concat("Did not connect to iSeries ", sErr))
        End Try

        Return program
    End Function
    Public Function GetSearchItems(ByVal loguser As String, ByVal PolType As String, ByVal Policy As String, ByVal LastName As String,
                                   ByVal FirstName As String, ByVal City As String, ByVal State As String, ByVal zip As String, ByVal Vin As String,
                                   ByVal LossDate As String) As String

        Dim stringConverter As cwbx.StringConverter = New cwbx.StringConverter 'Class() 20170705
        Dim strResult As String

        Dim program As cwbx.Program = New cwbx.Program()
        program = SetupPolicyConnection("Search")

        Dim parms As ProgramParameters = New ProgramParameters()
            'stringConverter.Length = 120
            parms.Clear()
            parms.Append("PolType", cwbrcParameterTypeEnum.cwbrcInput, 2)
            parms.Append("Policy", cwbrcParameterTypeEnum.cwbrcInput, 9)
            parms.Append("LastName", cwbrcParameterTypeEnum.cwbrcInput, 30)
            parms.Append("FirstName", cwbrcParameterTypeEnum.cwbrcInput, 30)
            parms.Append("City", cwbrcParameterTypeEnum.cwbrcInput, 20)
            parms.Append("State", cwbrcParameterTypeEnum.cwbrcInput, 2)
            parms.Append("Zip", cwbrcParameterTypeEnum.cwbrcInput, 9)
            parms.Append("Vin", cwbrcParameterTypeEnum.cwbrcInput, 17)
            parms.Append("LossDate", cwbrcParameterTypeEnum.cwbrcInput, 10)
            parms.Append("Response", cwbrcParameterTypeEnum.cwbrcOutput, 25000)


            parms("PolType").Value = stringConverter.ToBytes(PolType.ToUpper)
            parms("Policy").Value = stringConverter.ToBytes(Policy)
            parms("LastName").Value = stringConverter.ToBytes(LastName.ToUpper)
            parms("FirstName").Value = stringConverter.ToBytes(FirstName.ToUpper)
            parms("City").Value = stringConverter.ToBytes(City.ToUpper)
            parms("State").Value = stringConverter.ToBytes(State.ToUpper)
            parms("Zip").Value = stringConverter.ToBytes(zip.ToUpper)
            parms("Vin").Value = stringConverter.ToBytes(Vin.ToUpper)
            parms("LossDate").Value = stringConverter.ToBytes(LossDate)

            program.Call(parms)

            strResult = stringConverter.FromBytes(parms("Response").Value)

        Return strResult


    End Function
    Public Function GetRetrieveItems(ByVal loguser As String, ByVal Policy As String, ByVal LossDate As String, ByVal ClaimNumber As String) As String

        'File.WriteAllText("c:\XMLs\" + "LogFile.txt", "Retrieve program top of function")
        Dim stringConverter As cwbx.StringConverter = New cwbx.StringConverter 'Class() 20170705
        Dim strResult As String
        Dim xmlResult As New XmlDocument

        Dim program As cwbx.Program = New cwbx.Program()
        'File.WriteAllText("c:\XMLs\" + "LogFile.txt", "Next call is to SetupPolicyConnection")
        program = SetupPolicyConnection("Retrieve")

        'File.WriteAllText("c:\XMLs\" + "LogFile.txt", "SetupPolicyConnection Completed")
        Dim parms As ProgramParameters = New ProgramParameters()
        'stringConverter.Length = 120
        parms.Clear()
        parms.Append("Policy", cwbrcParameterTypeEnum.cwbrcInput, 9)
        parms.Append("LossDate", cwbrcParameterTypeEnum.cwbrcInput, 30)
        parms.Append("ClaimNumber", cwbrcParameterTypeEnum.cwbrcInput, 12)
        parms.Append("Response", cwbrcParameterTypeEnum.cwbrcOutput, 1048576)

        'File.WriteAllText("c:\XMLs\" + "LogFile.txt", "params")

        parms("Policy").Value = stringConverter.ToBytes(Policy)
        parms("LossDate").Value = stringConverter.ToBytes(LossDate)
        parms("ClaimNumber").Value = stringConverter.ToBytes(ClaimNumber)

        'File.WriteAllText("c:\XMLs\" + "LogFile.txt", "program.call")
        program.Call(parms)

        'File.WriteAllText("c:\XMLs\" + "LogFile.txt", "program returned, convert string")
        strResult = stringConverter.FromBytes(parms("Response").Value)

        Return strResult
    End Function
End Class
