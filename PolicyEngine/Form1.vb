﻿Imports System
Imports System.Xml
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.Odbc
Imports System.Text
Imports System.Security.Cryptography
Imports System.Xml.Serialization
Imports PolicyEngine.Search


Public Class Form1
    Public sParam As String

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Dim cmdLineArgs As String()
        'Dim Param1 As String
        cmdLineArgs = Environment.GetCommandLineArgs()

        If cmdLineArgs.Length > 1 Then ' no arguments, run normal send of DP shell setup
            sParam = cmdLineArgs(1).ToString()

            ' Call web service for activity creation when Pre-Inception changes to InForce occur - Jira 969 - WGN 20170501
            ' Runs against ODS_RPT, scheduled for once a day
            If sParam = "PreInceptionActivity" Then
                ProcessPreInception()
            End If

            ' Call web service for activity creation when CARCO changes occur - Jira 1496 - WGN 20170517
            ' Runs against ODS_RPT, scheduled for once a day
            If sParam = "CARCOActivity" Then
                ProcessCARCO()
            End If

            ' Test Stuff
            If sParam = "Test" Then
                TestPolicyRetrieve()
            End If

        Else ' Application mode set in appconfig file
            ' Call web service for activity creation when Pre-Inception changes to InForce occur - Jira 969 - WGN 20170501
            ' Runs against ODS_RPT, scheduled for once a day
            If My.Settings.appCommand = "PreInception" Then
                ProcessPreInception()
            End If

            ' Call web service for activity creation when CARCO changes occur - Jira 1496 - WGN 20170517
            ' Runs against ODS_RPT, scheduled for once a day
            If My.Settings.appCommand = "CARCO" Then
                ProcessCARCO()
            End If

            sParam = My.Settings.appCommand
        End If

        Close()
    End Sub
    Private Sub ProcessPreInception()
        ' Call web service for activity creation when Pre-Inception changes to InForce occur - Jira 969 - WGN 20170501
        ' Runs against ODS_RPT, scheduled for once a day
        Dim sEmail As String = My.Settings.eErrMail
        Try
            Dim queryString As String = "", queryString2 As String = ""
            Dim sXML As String = ""
            Dim sClaimNum As String = "", sClaimID As String
            Dim sPolicyNum As String = "", sLossDate As String = ""
            Dim sUserPublicID As String = "", sAssignedGroupID As String = "" ' - 20170417 WGN Note: AssignedGroupID is really the AssignedGroupPublicID
            Dim sServer As String = ""
            Dim sDatabase As String = "ODS_RPT"
            Dim sDueDays = 0 ' Due Date
            Dim sEscalateDays = 3 ' Escalation Date

            Dim sSearchPolicy As New Search
            Dim sSearchPolicyResult As String
            Dim sRetrievePolicyResult As String

            ' Select activity to all claims that are Pre-Inception state
            ' Send an assignment if 
            '   1. Status moved to "In Force"
            '   2. open an assignment for this issue if one does not exist
            '
            queryString = "SELECT c.id AS ClaimID, c.ClaimNumber, p.PolicyNumber, convert(varchar, convert(DATE, c.LossDate)) AS LossDate, u.PublicID as UserPublicID, g.PublicID as GroupPublicID " &
                          "FROM cc_policy p WITH (NOLOCK) " &
                          "JOIN cc_claim c WITH (NOLOCK) ON c.PolicyID = p.ID " &
                          "JOIN cc_user u WITH (NOLOCK) ON u.ID = c.AssignedUserID " &
                          "JOIN cc_group g WITH (NOLOCK) ON g.ID = c.AssignedGroupID " &
                          "WHERE p.PolicyStatusID = 10006 " & ' -- Pre-Inception -- CC Status 10006
                          "AND c.ClaimStateID = 2 " & ' -- Open Claims -- CC State
                          "ORDER BY c.ClaimNumber" ' - 20170510 WGN

            queryString2 = "SELECT a.ID, a.ActivityStatusID FROM cc_activity a WITH (NOLOCK) WHERE a.ShortSubject = 'PreIncept' AND a.ActivityStatusID = 1 AND a.ClaimID = " ' - WGN QA/Prod mode

            Dim objCreateActivitySvc As New PolicyEngine.GenActWService.PGfGenericActivityServiceService

            ' Set up server and activity access **Note: Designed to run against ODS once a day update, Mode should not be UAT 20170522 
            sServer = "PGCCLODST01"
            If My.Settings.mode = "DEV" Then
                objCreateActivitySvc.Url = "http://pgccldev01:7081/gw-service-bus-0.0.1-SNAPSHOT/CCGenericActivityService"
            ElseIf My.Settings.mode = "QA" Then
                objCreateActivitySvc.Url = "http://pgcclappt02:7081/gw-service-bus-0.0.1-SNAPSHOT/CCGenericActivityService"
            ElseIf My.Settings.mode = "UAT" Then
                sServer = "PGCCLAPPU01"
                objCreateActivitySvc.Url = "http://pgcclappu01:7480/gw-service-bus-0.0.1-SNAPSHOT/CCGenericActivityService"
            ElseIf My.Settings.mode = "PROD" Then
                sServer = "PGCCLODS01"
                objCreateActivitySvc.Url = "https://pgcclapp01:7480/CCGenericActivityService"
            End If

            LogToDB(ClassName + "." + MethodName, sServer + " Start - Processing Pre-Inception record", "Function call", "", CType(LogLevel.Informational, String), "Calling datareader for Pre-Inception claims " + queryString, sParam) ' Begin processing
            Dim dataReader As SqlDataReader = ExecuteSelect(sServer, sDatabase, queryString) '- Get Pre-Inception records

            If dataReader.HasRows Then
                Do While dataReader.Read()

                    sClaimID = If(IsDBNull(dataReader("ClaimID")), "", dataReader("ClaimID"))
                    sClaimNum = If(IsDBNull(dataReader("ClaimNumber")), "", dataReader("ClaimNumber"))
                    sPolicyNum = If(IsDBNull(dataReader("PolicyNumber")), "", dataReader("PolicyNumber"))
                    sLossDate = If(IsDBNull(dataReader("LossDate")), "", dataReader("LossDate"))
                    sUserPublicID = If(IsDBNull(dataReader("UserPublicID")), "", dataReader("UserPublicID")) ' - 20170417 WGN
                    sAssignedGroupID = If(IsDBNull(dataReader("GroupPublicID")), "", dataReader("GroupPublicID")) ' - 20170425 WGN
                    LogToDB(ClassName + "." + MethodName, sServer + "-" + sClaimNum + "-" + sPolicyNum + "-" + sLossDate + " Processing Pre-Inception record", "Function call", "", CType(LogLevel.Informational, String), "Calling datareader for Pre-Inception claims " + queryString2 + " " + sClaimID, sParam)

                    ' Ping the iSeries policy for updated status change 
                    sSearchPolicyResult = sSearchPolicy.GetRetrieveItems(System.Environment.UserName, sPolicyNum, sLossDate, sClaimNum)
                    LogToDB(ClassName + "." + MethodName, sServer + "-" + sClaimNum + "-" + sPolicyNum + "-" + sLossDate + " Found Policy ", "Function call", "", CType(LogLevel.Informational, String), "Calling searchPolicy to pull policy-" + LTrim(RTrim(sSearchPolicyResult)), sParam) ' Additional logging iSeries connection issues 20170713 (Renee')
                    sRetrievePolicyResult = RTrim(sSearchPolicyResult.Substring(403, 20)) ' Strip out the policy status
                    LogToDB(ClassName + "." + MethodName, sServer + "-" + sClaimNum + "-" + sPolicyNum + "-" + sLossDate + " Policy Status ", "Function call", "", CType(LogLevel.Informational, String), "Extracting policy status-" + sPolicyNum + "-" + LTrim(RTrim(sRetrievePolicyResult)), sParam) ' Additional logging iSeries connection issues 20170713 (Renee')
                    If sSearchPolicyResult.Substring(13, 1) = " " Then
                        If (sRetrievePolicyResult = "inforce") Then
                            Dim dataReader2 As SqlDataReader = ExecuteSelect(sServer, sDatabase, queryString2 + " " + sClaimID)
                            If Not dataReader2.HasRows Then ' An open assignment does not exists already
                                LogToDB(ClassName + "." + MethodName, sServer + "-" + sClaimNum + "-" + sPolicyNum + "-" + sLossDate + " Activity " + sRetrievePolicyResult + " record", "Function call", "", CType(LogLevel.Informational, String), "Calling datareader for Pre-Inception claims " + queryString2 + " " + sClaimID, sParam) ' Create Activity
                                Dim objActivityToSend As New PolicyEngine.GenActWService.Activity
                                objActivityToSend.ClaimNumber = sClaimNum
                                objActivityToSend.Mandatory = True
                                objActivityToSend.MandatorySpecified = True
                                objActivityToSend.ActivityPatternCode = "general_reminder"
                                objActivityToSend.ClaimID = sClaimID
                                objActivityToSend.AssignedUserPublicID = sUserPublicID ' - 20170414 WGN 
                                objActivityToSend.AssignedGroupID = sAssignedGroupID ' - 20170424 WGN 
                                objActivityToSend.Importance = objActivityToSend.Importance.high
                                objActivityToSend.ImportanceSpecified = True
                                objActivityToSend.Priority = GenActWService.Priority.high ' - WGN
                                objActivityToSend.PrioritySpecified = True ' - WGN
                                objActivityToSend.Subject = "Policy status - InForce"
                                objActivityToSend.ShortSubject = "PreIncept"
                                objActivityToSend.Description = "UW has changed the policy status. " + sPolicyNum + " Refresh policy."
                                objActivityToSend.TargetDate = DateAdd(DateInterval.Day, sDueDays, Date.Now)
                                objActivityToSend.TargetDateSpecified = True ' - WGN
                                objActivityToSend.EscalationDate = DateAdd(DateInterval.Day, sEscalateDays, Date.Now)
                                objActivityToSend.EscalationDateSpecified = True ' - WGN
                                objActivityToSend.Status = GenActWService.ActivityStatus.open

                                sXML = SerializeObject(objActivityToSend, objActivityToSend.GetType())
                                LogToDB(ClassName + "." + MethodName, sServer + ", " + sClaimNum + "-" + sPolicyNum + " Pre-Inception Processing Activity Created", "Function call", "", CType(LogLevel.Informational, String), "Pre-Inception - " + sXML, sParam)

                                Dim Response As New PolicyEngine.GenActWService.Response
                                Response = objCreateActivitySvc.createActivity(objActivityToSend)

                                If Response.Outcome <> 0 Then
                                    LogToDB(ClassName + "." + MethodName, sServer + ", " + sClaimNum + "-" + sPolicyNum + " Error - Pre-Inception Processing", "Function call", "", CType(LogLevel.Error, String), "Error - Activity on Claim on " + sClaimNum + " was not able to be set. Please address manually for Claim Number " + sClaimNum, sParam) ' Error activity
                                End If
                            Else
                                LogToDB(ClassName + "." + MethodName, sServer + " Skipped " + sClaimNum + "-" + sPolicyNum + " Pre-Inception open activity exists", "Function call", "", CType(LogLevel.Informational, String), "Calling datareader for Pre-Inception claims " + queryString2 + " " + sClaimID, sParam) ' Activity exists
                            End If

                            dataReader2.Close()

                            Threading.Thread.Sleep(100)
                            sXML = ""
                        End If
                    Else
                        ' Policy error - Log and continue
                        LogToDB(ClassName + "." + MethodName, sServer + ", " + sClaimNum + "-" + sPolicyNum + " Error - Policy Processing", "Function call", "", CType(LogLevel.Error, String), "Error - Policy - " + sSearchPolicyResult.Substring(10, 150), sParam)
                    End If
                Loop
            End If
            LogToDB(ClassName + "." + MethodName, sServer + " End - Processing Pre-Inception record", "Function call", "", CType(LogLevel.Informational, String), "Completed PreIncept Processing", sParam) ' Processing complete

            dataReader.Close()
        Catch ex As Exception
            LogToDB(ClassName + "." + MethodName, "Error: Pre-Inception Processing", "Function call", "", CType(LogLevel.Error, String), "Exception thrown " + ex.Message, sParam)
            SendWithStringAttachment(sEmail, sEmail, "", "Pre-Inception Processing Error", "Pre-Inception Processing exception " + ex.Message)
        End Try

    End Sub ' ProcessPreInception()
    Private Sub TestPolicyRetrieve()
        ' Call web service for activity creation when Pre-Inception changes to InForce occur - Jira 969 - WGN 20170501
        ' Runs against ODS_RPT, scheduled for once a day
        Dim sEmail As String = My.Settings.eErrMail
        Try
            Dim sClaimNum As String = "PA0004001653"
            Dim sClaimID As String
            Dim sPolicyNum As String = "NJ2476799" '"PN3242216" '"PN2892769" '"OR2022288" 
            Dim sLossDate As String = "2017-02-15" '"2017-07-02" '"2015-12-26" '"2014-10-28"
            Dim sServer As String = "PGCCLODST01"
            Dim sDatabase As String = "ODS_RPT"

            Dim sRetrievePolicy As New Search
            Dim sRetrievePolicyResult As String
            Dim sSearchPolicyResult As String

            Dim sPolicyNumber As String '275 - 9
            Dim sPolicyStatus As String '404 - 20
            Dim nPolTotVehNum As Short 'CC

            Dim nPolVehNumber(20) As Short
            Dim nPolVehVIN(20) As Short

            'Dim xPolicyResult As New XmlDocument

            ' Set up server and activity access **Note: Designed to run against ODS once a day update, Mode should not be UAT 20170522 

            If My.Settings.mode = "DEV" Then
                'objCreateActivitySvc.Url = "http://pgccldev01:7081/gw-service-bus-0.0.1-SNAPSHOT/CCGenericActivityService"
            ElseIf My.Settings.mode = "QA" Then
                'objCreateActivitySvc.Url = "http://pgcclappt02:7081/gw-service-bus-0.0.1-SNAPSHOT/CCGenericActivityService"
            ElseIf My.Settings.mode = "UAT" Then
                sServer = "PGCCLAPPU01"
                'objCreateActivitySvc.Url = "http://pgcclappu01:7480/gw-service-bus-0.0.1-SNAPSHOT/CCGenericActivityService"
            ElseIf My.Settings.mode = "PROD" Then
                sServer = "PGCCLODS01"
                'objCreateActivitySvc.Url = "https://pgcclapp01:7480/CCGenericActivityService"
            End If
            LogToDB(ClassName + "." + MethodName, sServer + " Start - Processing " + sParam + " record", "Function call", "", CType(LogLevel.Informational, String), "Started " + sParam, System.Environment.UserName + "/" + sParam) ' Begin processing

            LogToDB(ClassName + "." + MethodName, sServer + "-" + sClaimNum + "-" + sPolicyNum + "-" + sLossDate + " Processing " + sParam + " record", "Function call", "", CType(LogLevel.Informational, String), "Calling datareader for " + sParam + " claims" + " " + sClaimID, sParam)

            ' Ping the iSeries policy for updated status change 
            sRetrievePolicyResult = sRetrievePolicy.GetRetrieveItems(System.Environment.UserName, sPolicyNum, sLossDate, sClaimNum)

            'sSearchPolicyResult = sRetrievePolicy.GetSearchItems(System.Environment.UserName,, sPolicyNum,,,,,,, sLossDate)

            LogToDB(ClassName + "." + MethodName, sServer + "-" + sClaimNum + "-" + sPolicyNum + "-" + sLossDate + " Found Policy ", "Function call", "", CType(LogLevel.Informational, String), "Calling searchPolicy to pull policy-" + CType(Len(sRetrievePolicyResult) - Len(LTrim(sRetrievePolicyResult)) + 1, String) + "-" + LTrim(RTrim(sRetrievePolicyResult)), sParam) ' Additional logging iSeries connection issues 20170713 (Renee')
            sPolicyStatus = RTrim(sRetrievePolicyResult.Substring(403, 20)) ' Strip out the policy status
            LogToDB(ClassName + "." + MethodName, sServer + "-" + sClaimNum + "-" + sPolicyNum + "-" + sLossDate + " Policy Status ", "Function call", "", CType(LogLevel.Informational, String), "Extracting policy status-" + sPolicyNum + "-" + LTrim(RTrim(sPolicyStatus)), sParam) ' Additional logging iSeries connection issues 20170713 (Renee')
            If sRetrievePolicyResult.Substring(13, 1) = " " Then
            Else
                ' Policy error - Log and continue
                LogToDB(ClassName + "." + MethodName, sServer + ", " + sClaimNum + "-" + sPolicyNum + " Error - Policy Processing", "Function call", "", CType(LogLevel.Error, String), "Error - Policy - " + sRetrievePolicyResult.Substring(10, 150), sParam)
            End If
            LogToDB(ClassName + "." + MethodName, sServer + " End - Processing " + sParam + " record", "Function call", "", CType(LogLevel.Informational, String), "Completed " + sParam, sParam) ' Processing complete

        Catch ex As Exception
            LogToDB(ClassName + "." + MethodName, "Error: " + sParam + " Processing", "Function call", "", CType(LogLevel.Error, String), "Exception thrown " + ex.Message, sParam)
            SendWithStringAttachment(sEmail, sEmail, "", sParam + " Processing Error", sParam + " Processing exception " + ex.Message)
        End Try

    End Sub ' TestPolicyRetrieve()

    Private Sub ProcessCARCO()
        ' Call web service for activity creation when CARCO changes occur - Jira 1496 - WGN 20170517
        ' Runs against ODS_RPT, scheduled for once a day

        Dim sEmail As String = My.Settings.eErrMail
        Dim sServer As String = ""
        Dim sXML As String = ""
        Dim sErr As String = ""

        Try
            LogToDB(ClassName + "." + MethodName, "CARCO Updates", "Function call", "", CType(DebugLogLevel, String), "Start calling " + MethodName, sParam + "/" + Environment.UserName)
            Dim sRetrievePolicy As New Search
            Dim sRetrievePolicyResult As String
            Dim sSearchPolicyResult As String

            'Information from CC
            Dim sClaimNumber As String = "", sClaimID As String
            Dim sPolicyNumber As String = "", sLossDate As String = ""
            Dim sCarcoStatus As Integer, sCarcoDateRcv As String = ""
            Dim sVIN As String = "", sYear As Integer, sMake As String = "", sModel As String, sTotalPolVeh As Short

            'Information from Policy
            Dim cPolUnitIndex As Integer = 35187
            Dim cPolVINIndex As Integer = 1316, cPolVINLen As Integer = 17
            Dim cPolCarcoStatIndex As Integer = 36430, cPolCarcoStatLen As Integer = 2
            Dim cPolCaroDateIndex As Integer = 36432, cPolCarcoDateLen As Integer = 10

            Dim sPolicyNum As String = ""
            Dim sPolicyStat As String = "" '404 - 20
            Dim sPolVehVIN As String = "" 'Unit VIN
            Dim sPolVehCarcoStat As Integer 'Unit Carco Status
            Dim sPolVehCarcoDate As String = "" 'Unit Carco Date

            If (My.Settings.mode = "DEV" Or My.Settings.mode = "QA" Or My.Settings.mode = "UAT") Then
                sServer = "PGCCLODST01"
            ElseIf My.Settings.mode = "UAT" Then
                sServer = "PGCCLAPPU01"
            ElseIf My.Settings.mode = "PROD" Then
                sServer = "PGCCLODS01"
            End If

            For Each rec In ExecuteSQLSproc(sServer, "Operations", "usp_CCPullCARCOChanges", Nothing)
                ' Ping the iSeries policy for updated status change 
                sClaimNumber = If(IsDBNull(rec("ClaimNumber")), "", rec("ClaimNumber"))
                sClaimID = If(IsDBNull(rec("ClaimID")), "", rec("ClaimID"))
                sLossDate = If(IsDBNull(rec("LossDate")), "", rec("LossDate"))
                sPolicyNumber = If(IsDBNull(rec("PolicyNumber")), "", rec("PolicyNumber"))
                sCarcoStatus = If(IsDBNull(rec("CarcoStatusType")), "", rec("CarcoStatusType"))
                sCarcoDateRcv = If(IsDBNull(rec("CarcoDateRcv")), "", rec("CarcoDateRcv"))
                sVIN = If(IsDBNull(rec("VIN")), "", rec("VIN"))
                sYear = If(IsDBNull(rec("Year")), "", rec("Year"))
                sMake = If(IsDBNull(rec("Make")), "", rec("Make"))
                sModel = If(IsDBNull(rec("Model")), "", rec("Model"))
                sTotalPolVeh = If(IsDBNull(rec("TotalPolVeh")), "", rec("TotalPolVeh"))

                sRetrievePolicyResult = sRetrievePolicy.GetRetrieveItems(System.Environment.UserName, sPolicyNumber, sLossDate, sClaimNumber)

                If sRetrievePolicyResult.Substring(13, 1) = " " Then 'Pulled Policy
                    sPolicyNum = RTrim(sRetrievePolicyResult.Substring(274, 9)) ' Strip out the Policy Number
                    sPolicyStat = RTrim(sRetrievePolicyResult.Substring(403, 20)) ' Strip out the Policy Status

                    'If (sPolicyStat = "inforce") Then
                    For i = 0 To sTotalPolVeh - 1
                        sPolVehVIN = RTrim(sRetrievePolicyResult.Substring(i * cPolUnitIndex + cPolVINIndex, cPolVINLen))
                        sPolVehCarcoStat = RTrim(sRetrievePolicyResult.Substring(i * cPolUnitIndex + cPolCarcoStatIndex, cPolCarcoStatLen))
                        sPolVehCarcoDate = RTrim(sRetrievePolicyResult.Substring(i * cPolUnitIndex + cPolCaroDateIndex, cPolCarcoDateLen))
                        If sPolVehVIN = sVIN Then
                            Exit For
                        End If
                    Next

                    LogToDB(ClassName + "." + MethodName, sServer + "-" + sClaimNumber + "-" + sPolicyNumber + "-" + sLossDate + " Policy Status ", "Function call", "", CType(LogLevel.Informational, String), "Extracting policy status-" + sPolicyNumber, sParam + "/" + Environment.UserName) ' Additional logging iSeries connection issues 20170713 (Renee')
                    If sPolVehCarcoStat <> sCarcoStatus Then
                        LogToDB(ClassName + "." + MethodName, sServer + "-" + sClaimNumber + "-" + sPolicyNumber + "-" + sLossDate + " Policy Status ", "Function call", "", CType(LogLevel.Informational, String), "Creating activity-" + sPolicyNumber, sParam + "/" + Environment.UserName)
                        Dim objActivityToSend As New PolicyEngine.GenActWService.Activity()
                        objActivityToSend.ActivityPatternCode = "pg_carcoupdate"
                        'c.ClaimNumber, c.LossDate, pol.PolicyNumber,  veh.PG_CARCOStatus, veh.VIN 
                        'objActivityToSend.Subject = "CARCO Update Received " & rec("Year") & " " & rec("Make") & " " & rec("Model")
                        objActivityToSend.Subject = "CARCO Update Received " & sYear & " " & sMake & " " & sModel
                        objActivityToSend.ClaimNumber = sClaimNumber
                        objActivityToSend.ShortSubject = "CARCOUpd"
                        'objActivityToSend.Description = "VIN:" & rec("VIN") & ", CARCO Status:" & "4" & "," & vbCrLf & "Additional Information: CARCO Report was never received in the allotted time. COMP/COLL is Not valid for this claim."
                        objActivityToSend.Description = "VIN:" & sVIN & ", CARCO Status: " & sPolVehCarcoStat & ", " & vbCrLf & "Additional Information: CARCO Report is not necessary.  Please proceed with claim processing."
                        objActivityToSend.Importance = objActivityToSend.Importance.high
                        objActivityToSend.ImportanceSpecified = True
                        objActivityToSend.ClaimID = sClaimID

                        sXML = SerializeObject(objActivityToSend, objActivityToSend.GetType())

                        Dim objCreateActivitySvc As New PolicyEngine.GenActWService.PGfGenericActivityServiceService

                        If My.Settings.mode = "DEV" Then
                            objCreateActivitySvc.Url = "http://pgccldev01:8280/gw-service-bus-0.0.2-SNAPSHOT/CCGenericActivityService "
                            '"http://pgccldev01:8380/gw-service-bus-0.0.1-SNAPSHOT/CCGenericActivityService"
                        ElseIf My.Settings.mode = "QA" Then
                            objCreateActivitySvc.Url = "http://pgcclappt02:7081/gw-service-bus-0.0.1-SNAPSHOT/CCGenericActivityService"
                        ElseIf My.Settings.mode = "UAT" Then
                            objCreateActivitySvc.Url = "http://pgcclappu01:7480/gw-service-bus-0.0.1-SNAPSHOT/CCGenericActivityService"
                        ElseIf My.Settings.mode = "PROD" Then
                            objCreateActivitySvc.Url = "https://pgcclapp01:7480/CCGenericActivityService"
                        End If

                        'sXML = SerializeObject(objActivityToSend, objActivityToSend.GetType())
                        LogToDB(ClassName + "." + MethodName, sClaimNumber + ": Carco update Activity", "Function call", "", CType(LogLevel.Informational, String), "CARCO Activity " + sXML, sParam + "/" + Environment.UserName)

                        Dim Response As New PolicyEngine.GenActWService.Response
                        Response = objCreateActivitySvc.createActivity(objActivityToSend)

                        If Response.Outcome <> 0 Then
                            LogToDB(ClassName + "." + MethodName, "CARCO Processing", "Function call", "", CType(LogLevel.Error, String), "Activity on for CARCO update failed", sParam + "/" + Environment.UserName)
                        End If

                        Threading.Thread.Sleep(1000)
                        sXML = ""
                    End If
                    'End If
                End If

            Next

        Catch ex As Exception
            LogToDB(ClassName + "." + MethodName, "CARCO ERROR", "Function call", "", CType(LogLevel.Error, String), "Exception thrown " + ex.Message, sParam + "/" + Environment.UserName)
            SendWithStringAttachment(sEmail, sEmail, "", sParam + " Processing Error", sParam + "/" + Environment.UserName + " Processing exception " + ex.Message)
            Throw New ArgumentException(String.Concat("CARCO Activity processing ERROR " + ex.Message, sErr))
        End Try
        LogToDB(ClassName + "." + MethodName, "CARCO Updates", "Function call", "", CType(DebugLogLevel, String), "End calling " + MethodName, sParam + "/" + Environment.UserName)

    End Sub ' ProcessCARCO()
End Class
