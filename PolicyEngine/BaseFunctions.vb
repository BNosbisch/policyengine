﻿'Imports CC_APCheckProcessing.CC_IntegrationsEntities
Imports System.Net
Imports System.Net.Mail
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography


Public Module BaseFunctions


    Private connectionString As String
    Private Server As String


    Public Sub SetConnectionString(ByVal vServer As String, ByVal vDB As String)

        connectionString = "Data Source=" & vServer & ";Initial Catalog=" & vDB & ";integrated security=SSPI;"
    End Sub

    Public Sub SetConnectionString(ByVal vMode As String)
        Dim Server As String = ""
        Dim DB As String = ""
        If vMode = "DEV" Then
            Server = "PGCCLDBT01"
            DB = "PGIntegrationDB"
        ElseIf vMode = "QA" Then
            Server = "PGCCLAPPU01"
            DB = "PGIntegrationDB"
        ElseIf vMode = "PROD" Then
            Server = "PGCCLSQLPROD"
            DB = "PGIntegrationDB"
        End If
        connectionString = "Data Source=" & Server & ";Initial Catalog=" & DB & ";integrated security=SSPI;"
    End Sub


    '' ValidateSchema parameter value on/off
    'Public ReadOnly Property ValidateSchema As String
    '    Get
    '        Dim sValidateSchema As String = My.Settings.IsValidateSchema
    '        If (String.IsNullOrEmpty(sValidateSchema)) Then
    '            LogToDB(ClassName + "." + MethodName, "Exception Error", "Error", "", 1, "Validate Schema missing", "", "", -1)
    '        End If
    '        Return sValidateSchema
    '    End Get
    'End Property



    ' LogLevel enum used in logging implementation 
    Public Enum LogLevel
        'All Exceptions
        Severe = 1

        'While saving xml to shared folder
        [Error] = 2

        'When we get Reject Message from Mitchell ClaimSave WebService then we need to log error with Error Description that we get in returned XML.
        Warning = 3

        'After getting Successful response back from Mitchell Claim Save web services.  
        'After getting events Mitchell. We need to log an entry into log table with log description as Event Code.   
        Informational = 4

        'Before and After VB Method / function calls, Before calling Mitchell web services
        Debug = 5

        'All SP calls (send ProgramLevel (which is 6 defined under LogControl table))
        Trace = 6
    End Enum


    'returns number of rows affected
    Public Function ExecuteSQLSprocNQ(ByVal vSprocName As String, ByVal vParameters As SqlParameter()) As Integer
        Dim connection As New SqlConnection(connectionString)
        Dim command As SqlCommand = Nothing

        'Build SQL Command
        command = buildCommand(vSprocName, vParameters)

        command.CommandTimeout = 10800000 'set command time out to three hours

        'set & open connection
        command.Connection = connection
        connection.Open()

        'Execute Command & return results 
        Return command.ExecuteNonQuery

    End Function

    Public Function LogToDB(Optional ByRef LogSource As String = "", Optional ByRef LogType As String = "", Optional ByRef LogGroup As String = "",
                            Optional ByRef LogSubGroup As String = "", Optional ByRef LogLevel As Integer = 5, Optional ByRef LogMessage As String = "",
                            Optional ByRef LogUser As String = "", Optional ByRef LogKeyIDType As String = "", Optional ByRef LogKeyID As Integer = -1) As Integer

        Dim sServerLog As String = ""
        Dim sDatabaseName As String = ""
        Dim sLogSPName As String = ""
        If (My.Settings.mode = "DEV" Or My.Settings.mode = "QA" Or My.Settings.mode = "UAT") Then
            sServerLog = "PGCCLDBT01"
        ElseIf My.Settings.mode = "PROD" Then
            sServerLog = "PGCCLSQLPROD"
        End If

        sDatabaseName = "PGIntegrationDB"
        sLogSPName = "usp_PGIntAddLogging"

        SetConnectionString(sServerLog, sDatabaseName)
        Dim params(8) As SqlParameter

        params(0) = New SqlParameter
        params(0).ParameterName = "@LogSource"
        params(0).SqlDbType = SqlDbType.VarChar
        params(0).Value = LogSource

        params(1) = New SqlParameter
        params(1).ParameterName = "@LogType"
        params(1).SqlDbType = SqlDbType.VarChar
        params(1).Value = LogType

        params(2) = New SqlParameter
        params(2).ParameterName = "@LogUser"
        params(2).SqlDbType = SqlDbType.VarChar
        params(2).Value = LogUser

        params(3) = New SqlParameter
        params(3).ParameterName = "@LogLevel"
        params(3).SqlDbType = SqlDbType.Int
        params(3).Value = LogLevel

        params(4) = New SqlParameter
        params(4).ParameterName = "@LogMessage"
        params(4).SqlDbType = SqlDbType.VarChar
        params(4).Value = LogMessage

        params(5) = New SqlParameter
        params(5).ParameterName = "@LogGroup"
        params(5).SqlDbType = SqlDbType.VarChar
        params(5).Value = LogGroup


        params(6) = New SqlParameter
        params(6).ParameterName = "@LogSubGroup"
        params(6).SqlDbType = SqlDbType.VarChar
        params(6).Value = LogSubGroup

        params(7) = New SqlParameter
        params(7).ParameterName = "@LogKeyIDType"
        params(7).SqlDbType = SqlDbType.VarChar
        params(7).Value = LogKeyIDType

        params(8) = New SqlParameter
        params(8).ParameterName = "@LogKeyID"
        params(8).SqlDbType = SqlDbType.Int
        params(8).Value = LogKeyID
        Try
            ExecuteSQLSprocNQ(sLogSPName, params)
        Catch ex As Exception
            Dim xxx As String = ex.Message
        End Try
        Return 1
    End Function

    '' PGCServer parameter value 
    'Public ReadOnly Property PGCServer As String
    '    Get
    '        Dim sPGCServer As String = My.Settings.PGCServer
    '        If (String.IsNullOrEmpty(sPGCServer)) Then
    '            LogToDB(ClassName + "." + MethodName, "Exception Error", "Error", "", 1, "PGC Server missing", "", "", -1)
    '        End If
    '        Return sPGCServer
    '    End Get
    'End Property

    ' Severe LogLevel (1) used in writeDetailedLoggingToDB() function to log all exceptions
    Public ReadOnly Property SevereLogLevel As Integer
        Get
            Dim iLogLevel As Integer = CType([Enum].Parse(GetType(LogLevel), LogLevel.Severe.ToString()), Integer)
            Return iLogLevel
        End Get
    End Property

    ' Errer LogLevel (2) used in writeDetailedLoggingToDB() function to log exception while saving xml to shared folder
    Public ReadOnly Property ErrorLogLevel As Integer
        Get
            Dim iLogLevel As Integer = CType([Enum].Parse(GetType(LogLevel), LogLevel.Error.ToString()), Integer)
            Return iLogLevel
        End Get
    End Property

    ' Warning LogLevel (3) used in writeDetailedLoggingToDB() function to log entry when we get Reject Message from Mitchell ClaimSave WebService.
    Public ReadOnly Property WarningLogLevel As Integer
        Get
            Dim iLogLevel As Integer = CType([Enum].Parse(GetType(LogLevel), LogLevel.Warning.ToString()), Integer)
            Return iLogLevel
        End Get
    End Property

    ' Informational LogLevel (4) used in writeDetailedLoggingToDB() function to log entry in below cases-
    ' After getting Successful response back from Mitchell Claim Save web services.  
    ' After getting events Mitchell. We need to log an entry into log table with log description as Event Code.   
    Public ReadOnly Property InformationalLogLevel As Integer
        Get
            Dim iLogLevel As Integer = CType([Enum].Parse(GetType(LogLevel), LogLevel.Informational.ToString()), Integer)
            Return iLogLevel
        End Get
    End Property

    ' Debug LogLevel (5) used in writeDetailedLoggingToDB() function to log entry in below case-
    ' Before and After VB Method / function calls, Before calling Mitchell web services
    Public ReadOnly Property DebugLogLevel As Integer
        Get
            Dim iLogLevel As Integer = CType([Enum].Parse(GetType(LogLevel), LogLevel.Debug.ToString()), Integer)
            Return iLogLevel
        End Get
    End Property

    ' Trace LogLevel (6) used in writeDetailedLoggingToDB() function to log traces
    Public ReadOnly Property TraceLogLevel As Integer
        Get
            Dim iLogLevel As Integer = CType([Enum].Parse(GetType(LogLevel), LogLevel.Trace.ToString()), Integer)
            Return iLogLevel
        End Get
    End Property

    ' ClassName used in writeDetailedLoggingToDB() function as LogSource parametr
    Public ReadOnly Property ClassName() As String
        Get
            Dim stackTrace As New Diagnostics.StackFrame(1)
            If (stackTrace IsNot Nothing) Then
                Return stackTrace.GetMethod().DeclaringType().ToString()
            Else
                Return String.Empty
            End If
        End Get
    End Property

    ' MethodName used in writeDetailedLoggingToDB() function as LogSource parametr
    Public ReadOnly Property MethodName() As String
        Get
            Dim stackTrace As New Diagnostics.StackFrame(1)
            If (stackTrace IsNot Nothing) Then
                Return stackTrace.GetMethod.Name
            Else
                Return String.Empty
            End If
        End Get
    End Property

    Public Function ExecuteSelect(ByVal vServer As String, ByVal vDB As String, ByVal vQueryString As String) As SqlDataReader
        'connectionString="metadata=res://*/ChecksDataModel.csdl|res://*/ChecksDataModel.ssdl|res://*/ChecksDataModel.msl;provider=System.Data.SqlClient
        ';provider connection string=&quot;data source=pgac06qa;initial catalog=CC_Integrations;persist security info=True;user id=sa;password=pgacpgac;multipleactiveresultsets=True;App=EntityFramework&quot;" providerName="System.Data.EntityClient"
        'Dim sConnection As String = "Data Source=" & vServer & ";Initial Catalog=" & vDB & ";integrated security=SSPI;"


        Dim sConnection As String = "Data Source=" & vServer & ";Initial Catalog=" & vDB & ";persist security info=True;user id=odsreader;password=odsselect;"
        If vServer = "PGCCLAPPU01" Then
            'sConnection = "Data Source=" & vServer & ";Initial Catalog=" & vDB & ";integrated security=SSPI;"
            sConnection = "Data Source=" & vServer & ";Initial Catalog=" & vDB & ";persist security info=True;user id=intuser;password=intuser;"
        End If

        If vServer = "PGAC06" Then
            sConnection = "Data Source=" & vServer & ";Initial Catalog=" & vDB & ";integrated security=SSPI;"
        End If

        If vServer = "PGAC06QA" Then
            sConnection = "Data Source=" & vServer & ";Initial Catalog=" & vDB & ";integrated security=SSPI;"
        End If

        If vServer = "PGCCLSQLPROD" Then
            sConnection = "Data Source=" & vServer & ";Initial Catalog=" & vDB & ";integrated security=SSPI;"
        End If

        If vServer = "PGCCLODST01" Then
            sConnection = "Data Source=" & vServer & ";Initial Catalog=" & vDB & ";integrated security=SSPI;"
        End If

        If vServer = "PGCCLODS01" Then
            sConnection = "Data Source=" & vServer & ";Initial Catalog=" & vDB & ";integrated security=SSPI;"
        End If

        Dim connection As New SqlConnection(sConnection)

        Dim command As New SqlCommand(vQueryString, connection)

        connection.Open()

        Return (command.ExecuteReader())

    End Function

    Public Sub ExecuteNonSelect(ByVal vServer As String, ByVal vDB As String, ByVal vQueryString As String)
        Dim sConnection As String = "Data Source=" & vServer & ";Initial Catalog=" & vDB & ";integrated security=SSPI;"

        Using connection As New SqlConnection(sConnection)
            Dim command As New SqlCommand(vQueryString, connection)
            command.Connection.Open()
            command.CommandTimeout = 60
            command.ExecuteNonQuery()
        End Using

    End Sub

    Public Function ExecuteSQLSproc(ByVal vSprocName As String, ByVal vParameters As SqlParameter()) As SqlDataReader
        Dim connection As New SqlConnection(connectionString)
        Dim command As SqlCommand = Nothing

        'Build SQL Command
        command = buildCommand(vSprocName, vParameters)

        'set & open connection
        command.Connection = connection
        connection.Open()

        'Execute Command & return results 
        Return command.ExecuteReader

    End Function


    Public Function ExecuteSQLSproc(ByVal vServer As String, ByVal vDB As String, ByVal vSprocName As String, ByVal vParameters As SqlParameter()) As SqlDataReader
        Dim sConnStr As String = "Data Source=" & vServer & ";Initial Catalog=" & vDB & ";integrated security=SSPI;"
        Dim connection As New SqlConnection(sConnStr)
        Dim command As SqlCommand = Nothing

        'Build SQL Command
        command = buildCommand(vSprocName, vParameters)

        'set & open connection
        command.Connection = connection
        connection.Open()

        'Execute Command & return results 
        Return command.ExecuteReader

    End Function

    Private Function buildCommand(ByVal vSprocName As String, ByVal vParameters As SqlParameter()) As SqlCommand
        Dim command As New SqlCommand
        Dim tempParam As SqlParameter = Nothing

        'Set Command Parameters
        command.CommandText = vSprocName
        command.CommandType = CommandType.StoredProcedure
        command.CommandTimeout = 180 'set command timeout to 3 minutes

        'Add parameters to command
        If Not vParameters Is Nothing Then
            For Each tempParam In vParameters
                command.Parameters.Add(tempParam)
            Next
        End If

        Return command

    End Function

    ' This function will send mail to mailing list from the mailing list with the file name that is sent in
    ''' <returns>boolean value indicating mail has been sent succesfully or not
    ''' True - Mail sent successfully
    ''' False - Mail has not been sent and issue appears
    ''' </returns>
    Public Function SendWithStringAttachment(ByVal ToMailingList As String, ByVal FromMailingList As String,
                                             ByVal AttachmentFileName As String, ByVal Subject As String, ByVal Message As String) As Boolean

        Dim MailServer As String = "mail.pgac.com"
        Dim Port As String = "25"
        Dim Result As Boolean = False
        Dim Attachment As Mail.Attachment

        Try
            Dim MessageToMail As New MailMessage(FromMailingList, ToMailingList, Subject, Message)

            If AttachmentFileName.Length > 0 Then
                Attachment = New Mail.Attachment(AttachmentFileName)
                MessageToMail.Attachments.Add(Attachment)
            End If

            Dim mySmtpClient As New SmtpClient(MailServer, Port)
            mySmtpClient.UseDefaultCredentials = True
            mySmtpClient.Send(MessageToMail)

            Result = True
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Result = False
        End Try


        Return Result
    End Function


    Public Function SerializeObject(ByVal SerializableObject As Object, ByVal ObjectType As Type) As String

        Dim objXmlSer As New XmlSerializer(ObjectType)
        Dim objMemoryStream As New MemoryStream()
        Dim objXmlTextWriter As New XmlTextWriter(objMemoryStream, System.Text.Encoding.UTF8)
        objXmlTextWriter.Formatting = Formatting.Indented
        objXmlSer.Serialize(objXmlTextWriter, SerializableObject)
        objMemoryStream = CType(objXmlTextWriter.BaseStream, MemoryStream)
        Dim sXmlData As String = System.Text.Encoding.UTF8.GetString(objMemoryStream.ToArray())
        If (sXmlData.StartsWith("")) Then
            sXmlData = sXmlData.Remove(0, 1)
        End If

        Return sXmlData
    End Function



    Public Function SoapSerializeObject(ByVal SerializableObject As Object, ByVal ObjectType As Type) As String
        'Dim myTypeMapping As XmlTypeMapping = (New SoapReflectionImporter().ImportTypeMapping(objActivityToSend.GetType()))

        '      Dim mySerializer As XmlSerializer = New XmlSerializer(myTypeMapping)

        Dim objXmlSer As New XmlSerializer(ObjectType)
        Dim objMemoryStream As New MemoryStream()
        Dim objXmlTextWriter As New XmlTextWriter(objMemoryStream, System.Text.Encoding.UTF8)
        objXmlTextWriter.Formatting = Formatting.Indented
        objXmlSer.Serialize(objXmlTextWriter, SerializableObject)
        objMemoryStream = CType(objXmlTextWriter.BaseStream, MemoryStream)
        Dim sXmlData As String = System.Text.Encoding.UTF8.GetString(objMemoryStream.ToArray())
        If (sXmlData.StartsWith("")) Then
            sXmlData = sXmlData.Remove(0, 1)
        End If

        Return sXmlData
    End Function


    Public NotInheritable Class Simple3Des
        Private DES As New DESCryptoServiceProvider
        Private TripleDES As New TripleDESCryptoServiceProvider


        Private Function TruncateHash(
        ByVal key As String,
        ByVal length As Integer) As Byte()

            Dim sha1 As New SHA1CryptoServiceProvider

            ' Hash the key. 
            Dim keyBytes() As Byte =
                System.Text.Encoding.Unicode.GetBytes(key)
            Dim hash() As Byte = sha1.ComputeHash(keyBytes)

            ' Truncate or pad the hash. 
            ReDim Preserve hash(length - 1)
            Return hash
        End Function



        Sub New(ByVal key As String)
            ' Initialize the crypto provider.
            Dim keyBypes() As Byte = UTF8Encoding.UTF8.GetBytes(key)
            Dim keyB() As Byte = System.Text.Encoding.Unicode.GetBytes(Left(key, 24))
            'DES.Key = TruncateHash(key, DES.KeySize \ 8)
            'DES.IV = TruncateHash("", DES.BlockSize \ 8)
            TripleDES.Key = UTF8Encoding.UTF8.GetBytes(Left(key, 24))
            TripleDES.Mode = CipherMode.ECB
            TripleDES.Padding = PaddingMode.PKCS7


        End Sub


        Public Function EncryptData(
        ByVal plaintext As String) As String

            ' Convert the plaintext string to a byte array. 
            Dim plaintextBytes() As Byte =
                System.Text.Encoding.Unicode.GetBytes(plaintext)

            ' Create the stream. 
            Dim ms As New System.IO.MemoryStream
            ' Create the encoder to write to the stream. 
            Dim encStream As New CryptoStream(ms,
                TripleDES.CreateEncryptor(),
                System.Security.Cryptography.CryptoStreamMode.Write)

            ' Use the crypto stream to write the byte array to the stream.
            encStream.Write(plaintextBytes, 0, plaintextBytes.Length)
            encStream.FlushFinalBlock()

            ' Convert the encrypted stream to a printable string. 
            Return Convert.ToBase64String(ms.ToArray)
        End Function


        Public Function DecryptData(
        ByVal encryptedtext As String) As String

            ' Convert the encrypted text string to a byte array. 
            Dim encryptedBytes() As Byte = Convert.FromBase64String(encryptedtext)

            ' Create the stream. 
            Dim ms As New System.IO.MemoryStream
            ' Create the decoder to write to the stream.  
            Dim decStream As New CryptoStream(ms, TripleDES.CreateDecryptor(),
                System.Security.Cryptography.CryptoStreamMode.Write)

            ' Use the crypto stream to write the byte array to the stream.
            decStream.Write(encryptedBytes, 0, encryptedBytes.Length)
            decStream.FlushFinalBlock()

            ' Convert the plaintext stream to a string. 
            Return System.Text.Encoding.Unicode.GetString(ms.ToArray)
        End Function
    End Class








    ' Downloads file from ftp site
    Public Function DownloadFTP(ByVal vftpSite As String, ByRef vUserName As String, ByVal vPswd As String,
                                ByVal vlocalPath As String, ByVal vremotePath As String,
                                 ByVal vlocalFile As String, ByVal vremoteFile As String, ByVal bRemoveRemoteFile As Boolean
                                 ) As Boolean

        Dim bRet As Boolean = False
        Dim sErr As String

        Dim URI As String = vftpSite & vremotePath & vremoteFile

        Dim ftp As System.Net.FtpWebRequest =
            CType(FtpWebRequest.Create(URI), FtpWebRequest)

        ftp.Credentials = New _
            System.Net.NetworkCredential(vUserName, vPswd)

        ftp.KeepAlive = False

        ftp.UseBinary = True

        ftp.Method = System.Net.WebRequestMethods.Ftp.DownloadFile

        Try
            Using response As System.Net.FtpWebResponse =
                  CType(ftp.GetResponse, System.Net.FtpWebResponse)
                Using responseStream As IO.Stream = response.GetResponseStream

                    Using fs As New IO.FileStream(vlocalPath & vlocalFile, IO.FileMode.Create)
                        Dim buffer(2047) As Byte
                        Dim read As Integer = 0
                        Do
                            read = responseStream.Read(buffer, 0, buffer.Length)
                            fs.Write(buffer, 0, read)
                        Loop Until read = 0
                        responseStream.Close()
                        fs.Flush()
                        fs.Close()
                    End Using
                    responseStream.Close()
                End Using
                response.Close()
                bRet = True
            End Using

            bRet = True ' set return from function to true if no errors received only

        Catch ex As Exception
            sErr = Err.Description
        End Try


        ' IF WE ARE 3RD PARTY MITCHELL AND PA0002 THEN DO NOT REMOVE THE FILE, and leave on ftp for processing by CAP
        If vftpSite = "ftp://permgensftp.mitchell.com/" Then
            Dim lines As StreamReader
            Dim line As String
            Dim Filename As String = ""
            Dim EndOfLine As String = ""

            Try
                lines = New StreamReader(vlocalPath & vlocalFile)
                line = lines.ReadToEnd()
                lines.Close()
                lines.Dispose()
                If line.Contains("PA0002") Then
                    bRemoveRemoteFile = False ' since this is a ClaimCommander file, we will not remove it
                    File.Delete(vlocalPath & vlocalFile)   ' we can also delete it from the local location
                End If
            Catch ex As Exception
                sErr = Err.Description
            End Try
        End If

        If bRemoveRemoteFile And bRet Then ' when there is a rename file name, then rename it            
            Dim URI2 As String = vftpSite & vremotePath & vremoteFile

            Dim ftp2 As System.Net.FtpWebRequest = CType(FtpWebRequest.Create(URI2), FtpWebRequest)
            ftp2.Credentials = New System.Net.NetworkCredential(vUserName, vPswd)
            ftp2.UseBinary = True

            ftp2.Method = System.Net.WebRequestMethods.Ftp.DeleteFile
            Try
                Dim response2 As System.Net.FtpWebResponse

                response2 = CType(ftp2.GetResponse, System.Net.FtpWebResponse)
                Threading.Thread.Sleep(1000) ' wait one sec for delete to finish
                response2.Close()
            Catch ex As Exception
                sErr = "FTP Deletion of file failed with " & ex.Message
                LogToDB(ClassName + "." + MethodName, "FTP Download Error", "Function call", "", 1, sErr)
                bRet = False
            End Try

        End If

        Return bRet
    End Function


    Public Function GetListOfFilesFromFTPFolder(ByVal vftpSite As String, ByRef vUserName As String, ByVal vPswd As String,
                                ByVal vlocalPath As String, ByVal vremotePath As String,
                                 ByVal vlocalFilePattern As String, ByVal bRemoveFileAfterDownload As Boolean
                                 ) As Boolean

        Dim bRet As Boolean = False
        Dim sErr As String
        Dim strResp As String = ""

        Dim URI As String = vftpSite & vremotePath

        Dim ftp As System.Net.FtpWebRequest =
            CType(FtpWebRequest.Create(URI), FtpWebRequest)

        ftp.Credentials = New _
            System.Net.NetworkCredential(vUserName, vPswd)

        ftp.KeepAlive = False

        ftp.UseBinary = True
        ftp.Method = System.Net.WebRequestMethods.Ftp.ListDirectory

        Try

            Using response As System.Net.FtpWebResponse =
               CType(ftp.GetResponse, System.Net.FtpWebResponse)
                Using responseStream As IO.Stream = response.GetResponseStream

                    Using sr As New IO.StreamReader(responseStream)
                        Do While sr.Peek() >= 0
                            strResp = sr.ReadLine()
                            If strResp.StartsWith(vlocalFilePattern) = True Then
                                DownloadFTP(vftpSite, vUserName, vPswd, vlocalPath, vremotePath, strResp, strResp, bRemoveFileAfterDownload)
                            End If

                        Loop
                    End Using

                    responseStream.Close()
                End Using
                response.Close()
                bRet = True
            End Using

            bRet = True ' set return from function to true if no errors received only

        Catch ex As Exception
            sErr = ex.Message
            LogToDB(ClassName + "." + MethodName, "FTP Get List Error", "Function call", "", 1, sErr)
        End Try
        Return bRet
    End Function


    ' Downloads file from ftp site
    Public Function SendToFTP(ByVal vftpSite As String, ByRef vUserName As String, ByVal vPswd As String,
                                ByVal vlocalPath As String, ByVal vremotePath As String, ByVal vUploadAsFileName As String,
                                ByVal vSendFileName As String, ByVal vRenameFile As String
                                 ) As Boolean

        Dim bRet As Boolean = True
        Dim sErr As String

        Try

            Dim URI As String = vftpSite & vremotePath & vUploadAsFileName

            Dim ftp As System.Net.FtpWebRequest =
                CType(FtpWebRequest.Create(URI), FtpWebRequest)

            ftp.Credentials = New _
                System.Net.NetworkCredential(vUserName, vPswd)

            ftp.KeepAlive = False
            ftp.UseBinary = True

            ftp.Method = System.Net.WebRequestMethods.Ftp.UploadFile

            Dim file() As Byte = System.IO.File.ReadAllBytes(vlocalPath & vSendFileName)

            Dim strm As System.IO.Stream = ftp.GetRequestStream()
            strm.Write(file, 0, file.Length)
            strm.Close()
            strm.Dispose()

            Dim Response As System.Net.FtpWebResponse
            Try
                Response = CType(ftp.GetResponse(), System.Net.FtpWebResponse)
            Catch ex As Exception
                sErr = "FTP Error Sending file failed with " & ex.Message
                LogToDB(ClassName + "." + MethodName, "FTP Send Error", "Function call", "", 1, sErr)
                bRet = False
            End Try


            If vRenameFile.Length > 0 And bRet Then ' when there is a rename file name, then rename it                
                Dim URI2 As String = vftpSite & vremotePath & vUploadAsFileName

                Dim ftp2 As System.Net.FtpWebRequest = CType(FtpWebRequest.Create(URI2), FtpWebRequest)
                ftp2.Credentials = New System.Net.NetworkCredential(vUserName, vPswd)
                ftp2.UseBinary = True

                ftp2.Method = System.Net.WebRequestMethods.Ftp.Rename
                ftp2.RenameTo = vRenameFile

                Dim response2 As System.Net.FtpWebResponse
                Try
                    response2 = CType(ftp2.GetResponse, System.Net.FtpWebResponse)
                Catch ex As Exception
                    sErr = "FTP Renaming the file failed with " & ex.Message
                    LogToDB(ClassName + "." + MethodName, "FTP Send Error", "Function call", "", 1, sErr)
                    bRet = False
                End Try

            End If

        Catch ex As Exception
            sErr = "FTP Sending file failed with " & ex.Message
            LogToDB(ClassName + "." + MethodName, "FTP Send Error", "Function call", "", 1, sErr)
            bRet = False
        End Try

        Return bRet
    End Function









End Module
